# Planning

## TODO
```
  [] Offline First App
    [] Allow Sync to database if Online
    [] Allow Viewing of Data on Checkpoints - Encrypted QRCODE
    [] Generates Secure Encrypted QRCODE - need to be online for this
    [] Encryption Needs to be JWT Like but more secure
    [] All interaction should consider offline mobile
    [] Need Control Center Dashboard
    [] Need Mobile App
      [] Scanning QRCODE - Checkpoints
      [] Digital QRCODE for passes -> Control Center can Print Physical Copy

  [] Tools/Tech Needed
    [] Realm
    [] Mongodb 
    [] Fiber-prefork - Server API
    [] QRCODE
    [] ReactNative/Flutter

  [] QRCODE
    [] Data - Encrypted Information of Person/Household
            - Name of the Two Individuals attach to the Quarantine Pass
            - Preferred with Unique ID and ID number link to government or valid ID CARD
            - Control Number may have conflicts use Barangay-Zone/Purok-ControlNumber
  [] Server API
    [] Generates Encrypted QRCODE

  [] Web APP
    [] Control Center Dashboard

  [] Mobile APP
    [] Scan QRCODE
    [] Track Offline Recorded Data
    [] Sync to Server API if Online
    [] Digital Quarantine PASS.
```
